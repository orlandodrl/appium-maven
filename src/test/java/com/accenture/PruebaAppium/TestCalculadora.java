package com.accenture.PruebaAppium;

import java.net.MalformedURLException;
import java.net.URL;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class TestCalculadora {
	
	AppiumDriver driver;
    
    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "Huawei P8 Lite");
        capabilities.setCapability("udid", "G7T4C16928001281");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "7.0");
        capabilities.setCapability("appPackage", "com.android.calculator2");
        capabilities.setCapability("appActivity", "com.android.calculator2.Calculator");
        driver = new AppiumDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }
    
    @Test
    public void Calcular() {
    	
    	MobileElement digito2 = (MobileElement) driver.findElement(By.id("com.android.calculator2:id/digit2"));
        digito2.click();
        
        MobileElement suma = (MobileElement) driver.findElement(By.id("com.android.calculator2:id/plus"));
        suma.click();
        
        MobileElement digito8 = (MobileElement) driver.findElement(By.id("com.android.calculator2:id/digit8"));
        digito8.click();

        MobileElement igual = (MobileElement) driver.findElement(By.id("com.android.calculator2:id/equal"));
        igual.click();
        
        MobileElement pantalla = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
        System.out.println(pantalla.getText());
        
    }
     
    @After
    public void cerrar() {
        driver.quit();
    }
	
}
